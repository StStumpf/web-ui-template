# Softwareprojekt DHBW Mosbach Web UI Vorlage

## Abhängigkeiten

- Raspberry Pi installiert nach Vorgabe [Vorgabe](https://gitlab.com/ingoha/dhbw-raspi)
- [Node JS](https://nodejs.org/en/) v.10 oder höher
- MQTT Broker (bspw. [Mosquitto](https://mosquitto.org/))
- [MQTT Explorer](http://mqtt-explorer.com/)
- [Bootstrap Dokumentation](https://getbootstrap.com/docs/5.0/components/buttons/)


## Installation

Um alle Pakete auf dem System zu installieren kann der Befehl 

```
npm install
```

verwendet werden. Mit diesem werden alle in der Datei `package.json` definierten Abhängigkeiten installiert.


## Datenbank

Die Datenbank selbst sollte mit dem Installationsskript bereits vollständig installiert worden sein. Der Zugang sollte wie in dem Skript beschrieben über `mysql -u root -p123456` erfolgen können. Nach der Installation muss zunächst eine Datenbank erstellt werden. Dies gelingt mit dem Befehl: 

```sql 
/* Erstelle die Datenbank */
CREATE DATABASE WebMessages;
```

Danach kann diese ausgewählt werden. Alle folgenden Befehle werden dann in der Datenbank ausgeführt. Der Befehl dazu lautet `USE WebMessages;`.

Mittels des Befehls unterhalb kann dann die Tabelle, welche für das Template gebraucht wird erstellt werden.

```sql
/* Erstelle die Datenbank Tabelle */
CREATE TABLE `Messages` (
	`ID` INT NOT NULL AUTO_INCREMENT,
	`TimeStamp` DATETIME DEFAULT NOW(),
	`Message` TEXT,
	`Topic` TEXT,
	PRIMARY KEY (`ID`)
);
```

Dabei gilt es zu beachten, dass die Spalten `ID` und `TimeStamp` automatisch erstellt werden und bei einem `INSERT` nicht angegeben werden müssen. 

Alle Befehle finden Sie ebenfalls in der Datei `database-commands.sql`.


## Aufbau der Applikation

| Datei/Ordner        | Aufgabe           | Dateiart  |
| ------------- |-------------| -----|
| webserver.js      | Hosten eines Webservers, Hosten des WebSocketServers | JavaScript |
| html/*      | Root Ordner der Website | HTML/CSS/JavaScript |


## Starten der Anwendung

Zum Starten der Anwendung kann der Befehl `node webserver.js` oder alternativ `npm start` genutzt werden. Der Webserver ist dann unter `localhost:3000`, der Websocket unter `ws://localhost:8999` verfügbar. Alle Einstellungen finden Sie dazu in der Datei `webserver.js`.