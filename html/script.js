const hostname = window.location.hostname;

const webSocketUrl = `ws://${hostname}:8999`;
const ws = new WebSocket(webSocketUrl);

const messageContainer = document.getElementById("message-container");
const buttonGray = document.getElementById("button-gray");
const buttonGreen = document.getElementById("button-green");
const buttonBlue = document.getElementById("button-blue");
const buttonRed = document.getElementById("button-red");

/**
 * Füge neue Nachricht zu der Liste mit letzten Nachrichten hinzu.
 *
 * @param newMessage Neue Nachricht, welche empfangen wurde
 */
addMessageToQueue = (newMessage, isLocalMessage = false) => {
  if (messageContainer.childNodes.length >= 4) {
    messageContainer.removeChild(messageContainer.childNodes[0]);
  }

  const divContainer = document.createElement("div");
  // Bootstrap Style
  divContainer.classList.add("card");
  // Margin am Ende
  divContainer.classList.add("mb-2");

  const dateTimeFooter = document.createElement("div");
  dateTimeFooter.classList.add("card-footer");
  dateTimeFooter.classList.add("text-muted");

  const contentBody = document.createElement("div");
  contentBody.classList.add("card-body");

  const showBadge = isLocalMessage
    ? '<span class="badge badge-warning">Lokal</span>'
    : '<span class="badge badge-success">MQTT</span>';
  dateTimeFooter.innerHTML = `Uhrzeit: ${new Date().toLocaleTimeString()}`;
  contentBody.innerHTML = `${showBadge} ${newMessage}`;
  divContainer.appendChild(contentBody);
  divContainer.appendChild(dateTimeFooter);
  messageContainer.appendChild(divContainer);
};

/**
 * Funktion, welche beim Klicken des roten Buttons abgearbeitet wird
 */
buttonRed.onclick = () => {
  console.log("Roter Knopf");
  axios
    .get("/api/now")
    .then((response) => {
      console.log(response.data);
      addMessageToQueue(response.data, true);
    })
    .catch(() => {
      console.log("Fehler beim Abrufen der API Methode");
    });
};

/**
 * Funktion, welche beim Klicken des grauen Buttons abgearbeitet wird
 */
buttonGray.onclick = () => {
  ws.send("led-none");
  console.log("WS: Grauer Knopf");
  addMessageToQueue("Grauer Knopf an WebSocket geschickt", true);
};

/**
 * Funktion, welche beim Klicken des grünen Buttons abgearbeitet wird
 */
buttonGreen.onclick = () => {
  ws.send("led-green");
  console.log("WS: Grüner Knopf");
  addMessageToQueue("Grüner Knopf an WebSocket geschickt", true);
};

/**
 * Funktion, welche beim Klicken des blauen Buttons abgearbeitet wird
 */
buttonBlue.onclick = () => {
  ws.send("led-blue");
  console.log("WS: Blauer Knopf");
  addMessageToQueue("Blauer Knopf an WebSocket geschickt", true);
};

/**
 * Funktion, welche beim Erfolgreichen Öffnen des Websockets abgearbeitet wird
 */
ws.onopen = function () {
  console.log(`Erfolgreich mit WebSocket verbunden. (URL = ${webSocketUrl})`);
  addMessageToQueue(
    `Erfolgreich mit WebSocket verbunden. (URL = ${webSocketUrl})`,
    true
  );
};

/**
 * Funktion die bei einer neuen Nachricht ausgeführt wird.
 *
 * @param messageEvent Event mit neuer Nachricht.
 */
ws.onmessage = function (messageEvent) {
  console.log(`Neue Nachricht empfangen "${messageEvent.data}"`);
  addMessageToQueue(messageEvent.data);
};
