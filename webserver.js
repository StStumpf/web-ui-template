// Imports
const express = require("express");
const http = require("http");
const WebSocket = require("ws");
const mqtt = require("mqtt");
const mariadb = require("mariadb");

// Konfiguration
// 0.0.0.0 bedeutet, dass der Webserver unter allen IP-Adressen des PCs erreicht werden kann.
// Im Falle 'localhost' würde dieser nur auf dem Gerät selbst zu erreichen sein, denn 'localhost' ist nur
// wie es der Name vermuten lässt lokal verfügbar.
const webUIHost = "0.0.0.0";
const webUIPort = 3000;
const webSocketPort = 8999;
// Ändern zu mqtt://localhost nach Installation von mosquitto
const mqttUrl = "mqtt://test.mosquitto.org";
const mqttReceiveTopic = "softwareprojekt/mqtt-to-ws";
const mqttSendTopic = "softwareprojekt/ws-to-mqtt";
const htmlFolderName = "html";
const databaseHost = "localhost";
const databaseName = "WebMessages";
const databaseUsername = "root";
const databasePassword = "123456";

// Globale Objekte

// Verbinde MQTTClient mit dem Server.
const mqttClient = mqtt.connect(mqttUrl);
const app = express();
const webServer = http.createServer(app);
const webSocketServer = new WebSocket.Server({ server: webServer });

// Datenbank Verbindung wird in Methode erzeugt
let databaseConnection = null;

/**
 * Sende eine Nachricht an alle verbundenen WebSocket Clients.
 *
 * @param message Nachricht, welche versendet werden soll.
 */
sendMessageToAllClients = (message) => {
  webSocketServer.clients.forEach((client) => {
    client.send(message);
  });
};

/**
 * Abonniert ein Topic mittels des MQTTClients.
 *
 * @param topic Topic, welches abonniert werden soll.
 */
mqttClientSubscribeToTopic = (topic) => {
  mqttClient.subscribe(topic, {}, (error, granted) => {
    if (granted !== null) {
      console.error(
        `Subscription erfolgreich erstellt. Topic = ${mqttReceiveTopic}`
      );
    } else {
      console.error("Subscription konnte nicht erstellt werden.");
    }
  });
};

/**
 * Verarbeite ankommende MQTT Nachricht
 *
 * @param topic Thema der ankommenden Nachricht
 * @param message Nachricht Inhalt
 */
handleMqttMessage = (topic, message) => {
  console.log(
    "Nachricht via WebSocket erhalten: %s",
    message.toString().trim()
  );
  sendMessageToAllClients(`MQTT Message: ` + message.toString());
};

/**
 * Verarbeite ankommende Websocket Nachricht
 *
 * @param message Nachricht welche empfangen wurde
 */
handleWebSocketMessage = (message) => {
  console.log("Nachricht via WebSocket erhalten: %s", message);
  // Nachricht an MQTT senden
  mqttClient.publish(mqttSendTopic, message);
  // Nachricht in Datenbank speichern
  databaseConnection.query(
    `INSERT INTO Messages(Topic, Message) VALUES('${mqttSendTopic}', '${message}');`
  );
};

/**
 * Erstelle Handler für MQTTClient und richte Subscription zu ausgewählten Themen ein.
 */
initMqttClient = () => {
  /**
   * Funktion, welche beim erfolgreichen Verbinden mit dem MQTT Server abgearbeitet wird.
   */
  mqttClient.on("connect", () => {
    console.log(`MQTT wurde erfolgreich verbunden. Adresse = ${mqttUrl}`);

    // Abonniere ausgewähltes Thema
    mqttClientSubscribeToTopic(mqttReceiveTopic);
  });

  /**
   * Funktion, welche bei einer neuen MQTT Nachricht in dem Subscription Topic ausgeführt wird.
   */
  mqttClient.on("message", (topic, message) =>
    handleMqttMessage(topic, message)
  );
};

/**
 * Initialisiere Einstellungen des Webservers
 */
initWebServer = () => {
  // Aktiviere statische Dateien in Express (HTML, JS, CSS, ...)
  // Setze Quellverzeichnis auf ausgewähltes Verzeichnis
  app.use(express.static(htmlFolderName));

  // GET Endpunkt für aktuelle Uhrzeit
  app.get("/api/now", (request, response) => {
    return response.send(`${new Date()}`);
  });

  // GET Endpunkt für Liste mit letzten x Ergebnissen.
  app.get("/api/led-events", async (request, response) => {
    const limit = Number(request.query.limit) || 10;
    let results = [];

    await databaseConnection
      .query(`SELECT * FROM Messages ORDER BY TimeStamp DESC LIMIT ${limit}`)
      .then((res) => (results = res));

    return response.send(results);
  });
};

/**
 * Starte Webserver unter ausgewähltem Port
 */
startWebServer = () => {
  // Starte WebServer unter ausgewähltem Port
  app.listen(webUIPort, webUIHost, () => {
    console.log(
      `WebServer wurde gestartet. Adresse = http://${webUIHost}:${webUIPort}`
    );
  });
};

/**
 * Initialisiere Websocket-Server
 */
initWebsocketServer = () => {
  /**
   * Funktion, welche bei erfolgreichem Verbinden eines Clients ausgeführt wird.
   */
  webSocketServer.on("connection", (ws) => {
    /**
     * Funktion, welche bei einer Nachricht ausgeführt werden soll
     */
    ws.on("message", (message) => handleWebSocketMessage(message));

    // Verbindung wurde erfolgreich hergestellt. Client Feedback senden.
    ws.send("Verbindung erfolgreich.");
  });
};

/**
 * Starte Websocket-Server unter ausgewähltem Port
 */
startWebSocketServer = () => {
  // Starte Websocket-Server unter ausgewähltem Port
  webServer.listen(webSocketPort, webUIHost, () => {
    console.log(
      `WebSocket Server wurde gestartet. Adresse = http://${webUIHost}:${webSocketPort}`
    );
  });
};

/**
 * Verbinde zu Datenbank-Server
 */
initDatabaseConnection = async () => {
  await mariadb
    .createConnection({
      host: databaseHost,
      user: databaseUsername,
      password: databasePassword,
      database: databaseName,
    })
    .then((conn) => {
      console.log(
        `Datenbank (Host: ${databaseHost}) Verbindung wurde erfolgreich aufgebaut`
      );

      // Setze Verbindung, sodass diese in anderen Programmteilen verwendet werden kann.
      databaseConnection = conn;
    })
    .catch(() => {
      // Fehlerfall
      console.error(
        "Fehler. Datenbankverbindung konnte nicht aufgebaut werden."
      );

      // Beende Applikation
      process.exit();
    });
};

/**
 * Prüfe ob Datenbank erfolgreich verbunden und Datenbank sowie Tabelle korrekt angelegt sind.
 */
checkDatabaseConnection = async () => {
  await databaseConnection
    .query("SELECT COUNT(*) as Count FROM Messages")
    .then((results) =>
      // Zugriff Ergebnisse:
      // results ist Array von Ergebnis-Zeilen
      // Index 0, da wir nur eine Zeile erwarten.
      // Zugriff über Spaltenname: in diesem Fall Count (umbenannt mit 'as Count')
      console.log(
        `Test erfolgreich. Es wurden ${results[0].Count} Einträge gefunden!`
      )
    )
    .catch((error) => {
      console.error(error);
      // Fehlerfall
      console.error(
        "Datenbank Test nicht erfolgreich. Abbruch! Bitte prüfen ob alle Tabellen angelegt sind."
      );

      // Beende Applikation
      process.exit();
    });
};

/**
 * Main Methode
 */
main = async () => {
  initMqttClient();
  
  await initDatabaseConnection();
  await checkDatabaseConnection();

  initWebServer();
  initWebsocketServer();

  startWebServer();
  startWebSocketServer();
};

// Führe Main-Funktion aus, als Standard-Methode
main().catch((error) => {
  // Möglichkeit global auf einen Fehler zu reagieren.
  console.error(error);
  process.exit();
});
